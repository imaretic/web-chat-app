// Messaging logic
const messageForm = document.getElementById("message-form");
const messages = document.getElementById("messages");

messageForm.addEventListener("submit", (e) => {
  e.preventDefault();
  const message = e.target.message.value;
  if (!message) return;
  sendMessage(message);
  showMessage(message, true);
  e.target.message.value = "";
});

function packMessage(message) {
  return JSON.stringify({ clientId: clientId, message });
}

function showMessage(message, isMe) {
  const messageClassname = isMe ? `message message--me` : `message`;
  messages.innerHTML += `
    <div class="${messageClassname}"><span>${message}</span></div>
  `;
  gotoBottom(messages);
}

function gotoBottom(element) {
  element.scrollTop = element.scrollHeight - element.clientHeight;
}
