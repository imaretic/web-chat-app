document.getElementById("client-1").addEventListener("click", () => {
  clientId = 1;
  hideIdentitySelection();
});
document.getElementById("client-2").addEventListener("click", () => {
  clientId = 2;
  hideIdentitySelection();
});

document.getElementById("protocol-polling").addEventListener("click", () => {
  hideProtocolSelection();
  selectPolling();
});
document
  .getElementById("protocol-long-polling")
  .addEventListener("click", () => {
    hideProtocolSelection();
    selectLongPolling();
  });
document.getElementById("protocol-websocket").addEventListener("click", () => {
  hideProtocolSelection();
  selectWebsocket();
});

function hideProtocolSelection() {
  document.getElementById("protocol-selection").remove();
}
function hideIdentitySelection() {
  document.getElementById("identity-selection").remove();
}
