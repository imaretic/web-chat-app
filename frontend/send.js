function sendMessage(message) {
  switch (communicationType) {
    case "websocket":
      ws.send(packMessage(message));
      break;
    case "long-polling":
    case "polling":
      let xhr = new XMLHttpRequest();
      xhr.open("POST", `http://localhost:5000/messages/`);
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.onload = function () {
        // do something to response
        // or simply do nothing
      };
      xhr.send(packMessage(message));
      break;
    default:
      alert("No communication type selected!");
      break;
  }
}
