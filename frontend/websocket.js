// Initialize selected communication protocol
function selectWebsocket() {
  communicationType = "websocket";
  initWs();
}

function initWs() {
  ws = new WebSocket(`ws://${hostname}/${clientId}`);

  ws.onopen = function () {
    console.log("WebSocket connection established!");
  };
  ws.onclose = function () {
    console.log("WebSocket connection terminated!");
  };
  ws.onerror = function () {
    console.log("WebSocket connection error occured!");
  };

  ws.onmessage = function (messageEvent) {
    showMessage(messageEvent.data, false);
  };
}
