// Initialize selected communication protocol
function selectLongPolling() {
  communicationType = "long-polling";
  longPoll();
  poll();
  setInterval(poll, 10000);
}

function longPoll() {
  console.log("Checking for messages...");
  let xhr = new XMLHttpRequest();
  xhr.open("GET", `http://localhost:5000/messages-long-polling/${clientId}`);
  // xhr.timeout = 2000;
  xhr.onload = function () {
    let msg = this.response;
    if (this.status == 200 && msg) {
      showMessage(msg.slice(1, msg.length - 1), false);
    }
    longPoll();
  };
  xhr.timeout = 10000;
  xhr.send();
}
