// Initialize selected communication protocol
function selectPolling() {
  communicationType = "polling";
  poll();
  setInterval(poll, 2000);
}

function poll() {
  console.log("Poll...");
  let xhr = new XMLHttpRequest();
  xhr.open("GET", `http://localhost:5000/messages/${clientId}`);
  xhr.onload = function () {
    console.log(this.status, this.response);
    if (this.status == 200 && this.response) showMessage(this.response, false);
  };
  xhr.send();
}
