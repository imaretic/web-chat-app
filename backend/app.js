const express = require("express");
const http = require("http");
const WebSocket = require("ws");
const cors = require("cors");
const bodyParser = require("body-parser");
const expressLongpoll = require("express-longpoll");

const port = 5000;
let app = express();
var longpoll = expressLongpoll(app);
app.use(cors());
app.use(bodyParser.json());

const server = app.listen(port, function () {
  console.log(`Server app is listening on ${port}!`);
});

const websocketServer = new WebSocket.Server({ server });

let cli1PenMsg = "";
let cli2PenMsg = "";

/**
 * Polling
 */

// Send message
app.post("/messages/", (req, res, next) => {
  const { clientId, message } = req.body;
  if (clientId === 1) cli2PenMsg = message;
  if (clientId === 2) cli1PenMsg = message;
  deliverMessages();
  deliverPollMessages();
  return res.status(200).send({ message: "Message received!" });
});

// Return message
app.get("/messages/:clientId", (req, res, next) => {
  const clientId = parseInt(req.params.clientId);
  if (clientId === 1) {
    res.send(cli1PenMsg);
    cli1PenMsg = "";
  }
  if (clientId === 2) {
    res.send(cli2PenMsg);
    cli2PenMsg = "";
  }
  next();
});

/**
 * Long Polling
 */

let cli1Polling = false;
let cli2Polling = false;

// Register long polling paths
longpoll.create("/messages-long-polling/1", (req, res, next) => {
  cli1Polling = true;
  next();
});
longpoll.create("/messages-long-polling/2", (req, res, next) => {
  cli2Polling = true;
  next();
});

async function deliverPollMessages() {
  if (cli1PenMsg && cli1Polling) {
    longpoll.publish("/messages-long-polling/1", cli1PenMsg);
    cli1Polling = false;
    cli1PenMsg = "";
  }
  if (cli2PenMsg && cli2Polling) {
    longpoll.publish("/messages-long-polling/2", cli2PenMsg);
    cli2Polling = false;
    cli2PenMsg = "";
  }
}

/**
 * Initialize Websockets
 */

let cli1WsConn;
let cli2WsConn;

websocketServer.on("connection", function connection(wsClient, req) {
  // Save client connections
  const wsClientId = parseInt(req.url.slice(1));
  if (wsClientId === 1) cli1WsConn = wsClient;
  else if (wsClientId === 2) cli2WsConn = wsClient;
  deliverMessages();

  wsClient.on("message", function incoming(messagePack) {
    const { clientId, message } = extractMessagePack(messagePack);

    // Save messages
    if (clientId === 1) cli2PenMsg = message;
    else if (clientId === 2) cli1PenMsg = message;

    deliverMessages();
    deliverPollMessages();
  });
});

/**
 * Deliver messages via Websocket
 */
function deliverMessages() {
  // console.log("Delivering messages");
  // console.log(
  //   "Client 1",
  //   cli1PenMsg,
  //   cli1WsConn && cli1WsConn.readyState
  // );
  // console.log(
  //   "Client 2",
  //   cli2PenMsg,
  //   cli2WsConn && cli2WsConn.readyState
  // );
  // Deliver messages if possible
  if (cli1PenMsg && cli1WsConn && cli1WsConn.readyState === WebSocket.OPEN) {
    cli1WsConn.send(cli1PenMsg);
    cli1PenMsg = "";
  }
  if (cli2PenMsg && cli2WsConn && cli2WsConn.readyState === WebSocket.OPEN) {
    cli2WsConn.send(cli2PenMsg);
    cli2PenMsg = "";
  }
}

/**
 * Parse incoming message pack
 */
function extractMessagePack(messagePack) {
  return JSON.parse(messagePack);
}
